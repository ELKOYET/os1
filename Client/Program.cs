﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace ClientProg
{
    class Start
    {
        static void Main(string[] args)
        {          
                Console.Write("Введите адрес сервера  ");
                IPAddress ip;
                if (!IPAddress.TryParse(Console.ReadLine(), out ip)) { Console.Clear(); Main(args); return; }
                Console.Write("Введите порт сервера  ");
                int serverPort;
                if (!int.TryParse(Console.ReadLine(), out serverPort)) { Console.Clear(); Main(args); return; }
                Client client = new Client();
                client.Connect(ip, serverPort);
                client.Start();
                Console.Write("Подключено!!\n");
              
        }
    }
    class Client
    {
        private AutoResetEvent ev = new AutoResetEvent(false);
        private IPEndPoint endPoint;
        private Socket socket;
       

        public void Connect(IPAddress ip, int port)
        {
            endPoint = new IPEndPoint(ip, port);
            socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            socket.Connect(endPoint);
        }

        public void Start()
        {

            //Для отправки
            new Thread(new ThreadStart(Send)).Start();

            //Для приема
            new Thread(new ThreadStart(Receive)).Start();

        }

        public void Send()
        {
            while (true)
            {
                try
                {
                    byte[] buffer = Encoding.Unicode.GetBytes(new Random().Next(71,75).ToString());

                    socket.Send(buffer);

                  
                    Thread.Sleep(new Random().Next(2900, 11000));
                }
                catch
                {
                    Console.WriteLine("Что то пошло не так ");
                    break;
                }
            }
        }

        public void Receive()
        {
            while (true)
            {
                try
                {
                    SocketError err;

                    var data = new byte[256]; 
                    socket.BeginReceive(data, 0, data.Length, SocketFlags.None, out err, ReceiveHandle, null);
                    ev.WaitOne();
                    var receivedString = Encoding.Unicode.GetString(data, 0, data.Length).Trim('\0');

                    Console.WriteLine("Сервер ответил: " + receivedString.ToString());
                }
                catch
                {
                    Console.WriteLine("Что то пошло не так ");
                    break;
                }
            }
        }

        private void ReceiveHandle(IAsyncResult ar)
        {
            ev.Set();
        }




    }
}