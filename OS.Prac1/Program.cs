﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ServerProg
{
    class Start
    {
        static void Main(string[] args)
        {
                Console.Write("Введите порт сервера  ");
                int serverPort;
                if (!int.TryParse(Console.ReadLine(), out serverPort)) { Console.WriteLine("Был введен НЕВЕРНЫЙ порт"); Main(args); return; }
                Server server = new Server();
                server.Start(serverPort);

                Console.Write("Я сказал стартуем!\n");
            }
        }
    }

    class Server
    {
        private Dictionary<string, Task<byte[]>> issue = new Dictionary<string, Task<byte[]>>();
        private AutoResetEvent eve = new AutoResetEvent(false);
        private int port;
        private IPEndPoint endPoint;
        private Socket socket;

        private List<Client> clients = new List<Client>();

        private void Initialize()
        {
            endPoint = new IPEndPoint(IPAddress.Parse("127.0.0.1"), port);
            socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
        }

        public void Start(int Port)
        {
            port = Port;
            Initialize();

            socket.Bind(endPoint);
            socket.Listen(6);

            Thread t = new Thread(new ThreadStart(Listening));
            t.Start();
        }
        public void Listening()
        {
            int id = 0;
            while (true)
            {
                Socket handler = socket.Accept();
                Client client = new Client { Id = id++, socket = handler, are = new AutoResetEvent(false) };
                clients.Add(client);
                Console.WriteLine($"Клиент номер  {client.Id} установлено соединение");
                new Thread(ReceiveAndAnswer).Start(client);

            }
        }
        private class Client
        {
            public int Id;
            public Socket socket;
            public AutoResetEvent are;
        }

        public void ReceiveAndAnswer(object state)
        {
            CancellationTokenSource cans = new CancellationTokenSource();
            Client client = (Client)state;
            Socket socket = client.socket;

            SocketError error;

            Task<byte[]> thistask = null;

            while (true)
            {
            try
            {

                byte[] data = new byte[256];

                socket.BeginReceive(data, 0, data.Length, SocketFlags.None, out error, ReceiveHandle, client);
                client.are.WaitOne();

                var receivedString = Encoding.Unicode.GetString(data, 0, data.Length).Trim('\0');

                Console.Write($"Клиент номер : {client.Id} принято " + receivedString);
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("Время поступления: " + DateTime.Now.ToLongTimeString());
                Console.ResetColor();


                    
                    if (issue.ContainsKey(receivedString))
                    {
                        thistask = issue[receivedString];
                    }
                    else
                    {
                        if (thistask != null) cans.Cancel();
                        cans = new CancellationTokenSource();
                        thistask = Task.Factory.StartNew(() => ServerWork((receivedString, client.Id), cans.Token), cans.Token);
                        issue.Add(receivedString, thistask);
                    }

                    thistask.ContinueWith(t => { Send(client, t.Result); }, TaskContinuationOptions.NotOnCanceled);
                }
                catch
                {
                    break;
                }

            }
            Console.WriteLine($"Client№ {client.Id} Офнул");
        }


        private void ReceiveHandle(IAsyncResult ar)
        {
            (ar.AsyncState as Client).are.Set();
        }

        private byte[] ServerWork(object input, object token)
        {
            (string msg, int id) inpt = ((string msg, int id))input;
            try
            {
                var cancel = (CancellationToken)token;
                cancel.ThrowIfCancellationRequested();   
                var data = Encoding.Unicode.GetBytes((int.Parse(inpt.msg)*10).ToString());
                cancel.ThrowIfCancellationRequested();
                Thread.Sleep(6500);
                cancel.ThrowIfCancellationRequested();
                issue.Remove(inpt.msg);

                return data;
            }
            catch 
            {
                issue.Remove(inpt.msg);

                Console.WriteLine($"Клиент: {inpt.id} операция прервана ");
                return null;
            }
        }

        private void Send(object _client, object _data)
        {
            var client = ((Client)_client);
            var data = (byte[])_data;

          
            if (client != null && data != null)
                try
                {
                    client.socket.Send(data);
                }
                catch
                {
                    Console.WriteLine($"Клиент№ {client.Id} оторвался");
                }
        }
    }
